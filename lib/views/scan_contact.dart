import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart' show BarcodeScanner;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluffychat/components/matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../components/matrix.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:fluffychat/components/avatar.dart';

class ScanScreen extends StatefulWidget {
  @override
  _ScanState createState() => _ScanState();
}

class _ScanState extends State<ScanScreen> {
  String newchatID = '';
  String encryptedNewChatID;
  String base64encryptedNewChatID;
  String newUserName = '';
  String scanMessage =
      'Jemand lädt dich zu einem Chat mit der Hermannpost ein?';

  void submitAction(BuildContext context) async {
    final keyutf8 = await rootBundle.loadString('assets/keys/keyaes256cbc.txt');
    final ivutf8 = await rootBundle.loadString('assets/keys/ivaes256cbc.txt');
    final key = enc.Key.fromUtf8(keyutf8);
    final iv = enc.IV.fromUtf8(ivutf8);
    final encryptedNewChatID =
        enc.Encrypted.fromBase64(base64encryptedNewChatID);
    final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));
    final newchatID = encrypter.decrypt(encryptedNewChatID, iv: iv).trim();

    newUserName = newchatID.split(':').first;
    scanMessage = 'Verbinde mit ' + newUserName + '...';
    if (newchatID.isEmpty) return;

    final matrix = Matrix.of(context);

    if ('@' + newchatID == matrix.client.userID) return;

    var confirmed = false;
    final client = Matrix.of(context).client;
    await showDialog(
      context: context,
      builder: (c) => AlertDialog(
        title: Text('Neuer Chat?'),
        content: SingleChildScrollView(
          child: Container(
            child: FutureBuilder<Profile>(
                future:
                    Matrix.of(context).client.requestProfile('@' + newchatID),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) return CircularProgressIndicator();
                  final Profile profile = snapshot.data;
                  return Column(
                    children: [
                      Container(
                        child: Avatar(profile.avatarUrl, profile.displayname,
                            size: 160, client: client),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                        child: Text(
                          profile.displayname,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                      ),
                    ],
                  );
                }),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Nein'.toUpperCase(),
                style: TextStyle(color: Colors.blueGrey)),
            onPressed: () => Navigator.of(c).pop(),
          ),
          FlatButton(
            child: Text(
              'Ja'.toUpperCase(),
            ),
            onPressed: () {
              confirmed = true;
              Navigator.of(c).pop();
            },
          ),
        ],
      ),
    );

    if (confirmed == false) {
      return;
    }

    final user = User(
      '@' + newchatID,
      room: Room(id: '', client: matrix.client),
    );
    final roomID = await showFutureLoadingDialog(
      context: context,
      future: () => user.startDirectChat(),
    );

    if (roomID.error == null) {
      await AdaptivePageLayout.of(context)
          .popAndPushNamed('/rooms/${roomID.result}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Kontakt hinzufügen'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Image.asset('assets/private_chat_wallpaper.png'),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  scanMessage,
                  style: TextStyle(fontSize: 18),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  'Scanne die QR-Einladung von deinem neuen Kontakt!',
                  textAlign: TextAlign.center,
                ),
              ),
              Hero(
                tag: 'loginwithScan',
                child: Container(
                  width: double.infinity,
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: RaisedButton(
                      elevation: 7,
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6),
                      ),
                      onPressed: scan,
                      child: const Text(
                        'QR-Code scannen',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  newchatID,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ));
  }

  Future scan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() {
        base64encryptedNewChatID = barcode.rawContent;
      });
      submitAction(context);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          newchatID = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => newchatID = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => newchatID =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => newchatID = 'Unknown error: $e');
    }
  }
}
