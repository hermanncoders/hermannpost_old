import 'dart:async';
import 'dart:math';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffychat/components/matrix.dart';
import 'package:fluffychat/app_config.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:url_launcher/url_launcher.dart';
import '../utils/localized_exception_extension.dart';

class HomeserverPicker extends StatefulWidget {
  @override
  _HomeserverPickerState createState() => _HomeserverPickerState();
}

class _HomeserverPickerState extends State<HomeserverPicker> {
  bool _isLoading = false;
  final _domain = AppConfig.defaultHomeserver;
  final TextEditingController _controller =
      TextEditingController(text: AppConfig.defaultHomeserver);

  void _checkHomeserverAction(BuildContext context) async {
    try {
      if (_domain.isEmpty) throw L10n.of(context).changeTheHomeserver;
      var homeserver = _domain;

      if (!homeserver.startsWith('https://')) {
        homeserver = 'https://$homeserver';
      }

      setState(() => _isLoading = true);
      await Matrix.of(context).client.checkHomeserver(homeserver);
      final loginTypes = await Matrix.of(context).client.requestLoginTypes();
      if (loginTypes.flows
          .any((flow) => flow.type == AuthenticationTypes.password)) {
        await AdaptivePageLayout.of(context)
            .pushNamed(AppConfig.enableRegistration ? '/signup' : '/scanlogin');
      } else if (loginTypes.flows
          .any((flow) => flow.type == AuthenticationTypes.sso)) {
        await AdaptivePageLayout.of(context).pushNamed('/sso');
      }
    } on String catch (e) {
      // ignore: unawaited_futures
      FlushbarHelper.createError(message: e).show(context);
    } catch (e) {
      // ignore: unawaited_futures
      FlushbarHelper.createError(
              message: (e as Object).toLocalizedString(context))
          .show(context);
    } finally {
      if (mounted) {
        setState(() => _isLoading = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final padding = EdgeInsets.symmetric(
      horizontal: max((MediaQuery.of(context).size.width - 600) / 2, 0),
    );
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: padding,
          child: ListView(
            children: [
              Hero(
                tag: 'loginBanner',
                child: Image.asset('assets/hermannpost-banner.png'),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: padding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Hero(
              tag: 'loginButton',
              child: Container(
                width: double.infinity,
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: RaisedButton(
                  elevation: 7,
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: _isLoading
                      ? LinearProgressIndicator()
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset('assets/hp-qr.png',
                                  fit: BoxFit.cover),
                            ),
                            Text(
                              L10n.of(context).connect.toUpperCase(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          ],
                        ),
                  onPressed:
                      _isLoading ? null : () => _checkHomeserverAction(context),
                ),
              ),
            ),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                IconButton(
                  icon: Image.asset('assets/hp-legal.png', fit: BoxFit.cover),
                  onPressed: () =>
                      AdaptivePageLayout.of(context).pushNamed('/privacy'),
                ),
                IconButton(
                  icon: Image.asset('assets/hp-help.png', fit: BoxFit.cover),
                  onPressed: () =>
                      AdaptivePageLayout.of(context).pushNamed('/help'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
