import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'home_view_parts/chat_list.dart';
import '../components/adaptive_page_layout.dart';
import '../components/matrix.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:flutter/services.dart';

import 'dart:async';

class GenerateScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdaptivePageLayout(
      primaryPage: FocusPage.FIRST,
      firstScaffold: ChatList(),
      secondScaffold: GenerateScreen(),
    );
  }
}

class GenerateScreen extends StatefulWidget {
  @override
  _GenerateScreenState createState() => _GenerateScreenState();
}

class _GenerateScreenState extends State<GenerateScreen> {
  Future<dynamic> profileFuture;
  dynamic profile;
  String storedkey;
  String storediv;

  Future<String> readKeyUtf8() async {
    try {
      storedkey = await rootBundle.loadString('assets/keys/keyaes256cbc.txt');
    } catch (e) {
      print("Couldn't read file");
    }
    return storedkey;
  }

  Future<String> readIvUtf8() async {
    try {
      storediv = await rootBundle.loadString('assets/keys/ivaes256cbc.txt');
    } catch (e) {
      print("Couldn't read file");
    }
    return storedkey;
  }

  @override
  Widget build(BuildContext context) {
    var client = Matrix.of(context).client;
    profileFuture ??= client.ownProfile.then((p) {
      if (mounted) setState(() => profile = p);
      return p;
    });
    readKeyUtf8();
    readIvUtf8();
    final qr_text = client.userID.substring(1);
    final key = enc.Key.fromUtf8(storedkey);
    final iv = enc.IV.fromUtf8(storediv);
    final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));
    final encryptedQR = encrypter.encrypt(qr_text, iv: iv).base64;

    return Scaffold(
      appBar: AppBar(
        title: Text('QR-Einladung'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            Text(profile.displayname,
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'lädt dich zum Chatten mit der Hermannpost ein:',
                style: TextStyle(fontSize: 22),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              color: Colors.white,
              child: QrImage(
                data: encryptedQR,
                size: 300,
                gapless: true,
                errorCorrectionLevel: QrErrorCorrectLevel.Q,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Nimmst du die Einladung an? Dann gehe auf "Kontakt hinzufügen" und scanne den QR-Code!',
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
