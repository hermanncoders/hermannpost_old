import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart' show BarcodeScanner;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluffychat/components/matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffychat/components/dialogs/simple_dialogs.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../components/matrix.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:fluffychat/utils/shared_preferences.dart';

ProgressDialog pr;

class LoginScanScreen extends StatefulWidget {
  @override
  _LoginScanState createState() => _LoginScanState();
}

class _LoginScanState extends State<LoginScanScreen> {
  String usernameError;
  String passwordError;
  bool loading = false;
  String base64encryptedLoginID;
  String newloginID;
  String myloginID;
  String matrixID;
  String username;
  String password;
  String
      passwordCode; // TO DO: implement store password code to be able to change the PIN later
  String homeserver;
  String passwordPin;

  String loginScanMessage =
      'Scanne den QR-Code, den du von deiner Schule bekommen hast.';
  String loginScanImage = 'assets/hermannkey.png';

  void decrypt(BuildContext context) async {
    await StorageUtil.getInstance();
    final keyutf8 = await rootBundle.loadString('assets/keys/keyaes256cbc.txt');
    final ivutf8 = await rootBundle.loadString('assets/keys/ivaes256cbc.txt');
    final key = enc.Key.fromUtf8(keyutf8);
    final iv = enc.IV.fromUtf8(ivutf8);
    final encryptedLoginID = enc.Encrypted.fromBase64(base64encryptedLoginID);
    final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));
    final newloginID = encrypter.decrypt(encryptedLoginID, iv: iv);

    myloginID = newloginID;

    if (myloginID[0] == '1' ?? true) {
      myloginID = myloginID.substring(1);
      setState(() {
        StorageUtil.putString('usertype', 'teacher');
      });
    }

    matrixID = myloginID.split('*').first;
    password = myloginID.split('*').last;
    password = password.trim();
    homeserver = matrixID.split(':').last;
    username = matrixID.split(':').first;
    passwordPin = password.substring(8);

    loginScanImage = 'assets/scansuccess.png';
    loginScanMessage = 'Scan erfolgreich! Server wird angefragt, bitte warten!';

    if (homeserver == 'post.hermannschule.de' ?? true) {
      checkHomeserverAction(homeserver, context);
    } else {
      setState(() {
        loginScanImage = 'assets/wrongpin.png';
        loginScanMessage =
            'Der QR ist nicht gültig oder es gibt Probleme mit dem Server! Versuche es bitte gleich nochmal.';
      });
    }
  }

  void checkHomeserverAction(String homeserver, BuildContext context) async {
    if (!homeserver.startsWith('https://')) {
      homeserver = 'https://$homeserver';
    }
    final success =
        await checkHomeserver(homeserver, Matrix.of(context).client);

    if (success != false) {
      checkPin(context);
    } else {
      setState(() {
        loginScanImage = 'assets/badqr.png';
        loginScanMessage =
            'Der QR ist nicht gültig oder es gibt Probleme mit dem Server! Versuche es bitte gleich nochmal.';
      });
    }
  }

  void checkPin(BuildContext context) async {
    final pin = await SimpleDialogs(context).enterText(
      multiLine: false,
      titleText: 'Deine PIN?',
      labelText: 'PIN eingeben: ',
      hintText: '****',
      password: true,
      keyboardType: TextInputType.number,
    );
    if (pin == passwordPin) {
      login(context);
    } else {
      setState(() => loginScanImage = 'assets/wrongpin.png');
      loginScanMessage =
          'Die PIN ist falsch! Bitte nochmal versuchen oder eine neue PIN einfordern!';
    }
  }

  void login(BuildContext context) async {
    var matrix = Matrix.of(context);

    setState(() {
      // pr.show();
      setState(() => usernameError = null);
      setState(() => passwordError = null);
      loading = true;
      loginScanImage = 'assets/favicon.png';
      loginScanMessage = 'Willkommen! Du wirst in Kürze eingeloggt! ';
    });
    try {
      await matrix.client.login(
          user: username,
          password: password,
          initialDeviceDisplayName: matrix.clientName);
    } on MatrixException catch (exception) {
      setState(() => passwordError = exception.errorMessage);
      return setState(() => loading = false);
    } catch (exception) {
      setState(() => passwordError = exception.toString());
      return setState(() => loading = false);
    }

    if (mounted) setState(() => loading = false);
  }

  Timer _coolDown;

  void _checkWellKnownWithCoolDown(String userId, BuildContext context) async {
    _coolDown?.cancel();
    _coolDown = Timer(
      Duration(seconds: 1),
      () => _checkWellKnown(userId, context),
    );
  }

  void _checkWellKnown(String userId, BuildContext context) async {
    setState(() => usernameError = null);
    if (!userId.isValidMatrixId) return;
    try {
      final wellKnownInformations = await Matrix.of(context)
          .client
          .getWellKnownInformationsByUserId(userId);
      final newDomain = wellKnownInformations.mHomeserver?.baseUrl;
      if ((newDomain?.isNotEmpty ?? false) &&
          newDomain != Matrix.of(context).client.homeserver.toString()) {
        await showFutureLoadingDialog(
          context: context,
          future: () => Matrix.of(context).client.checkHomeserver(newDomain),
        );
        setState(() => usernameError = null);
      }
    } catch (e) {
      setState(() => usernameError = e.toString());
    }
  }

  Future<bool> checkHomeserver(dynamic homeserver, Client client) async {
    await client.checkHomeserver(homeserver);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mit QR-Scan einloggen'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Image.asset(loginScanImage),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  loginScanMessage,
                  style: TextStyle(fontSize: 18),
                  textAlign: TextAlign.center,
                ),
              ),
              Hero(
                tag: 'loginwithScan',
                child: Container(
                  width: double.infinity,
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: RaisedButton(
                      elevation: 7,
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6),
                      ),
                      onPressed: scan,
                      child: const Text(
                        'QR-Code scannen',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              ),
            ],
          ),
        ));
  }

  Future scan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() {
        base64encryptedLoginID = barcode.rawContent;

        loginScanImage = 'assets/scansuccess.png';
        loginScanMessage = 'Scannen hat geklappt';

        decrypt(context);
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          loginScanMessage =
              'Bitte erlaube zum Scannen den Zugriff auf die Kamera!';
        });
      } else {
        setState(() => loginScanMessage = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => loginScanMessage =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => loginScanMessage = 'Der Scan hat nicht geklappt: $e');
    }
  }
}
