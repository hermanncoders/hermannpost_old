import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PrivacyView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final url = 'https://hermannschule.de/hermannpost/Datenschutz.html';
    if (kIsWeb) launch(url);
    return Scaffold(
      appBar: AppBar(
        title: Text('Nutzungsbedingungen'),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: kIsWeb
                ? Center(child: Icon(Icons.link))
                : WebView(
                    initialUrl: url,
                    javascriptMode: JavascriptMode.unrestricted,
                  ),
          ),
        ],
      ),
    );
  }
}
