<img style="margin-left: auto; margin-right: auto;" src="https://hermannschule.de/hermannpost/hermannpost-banner.png">
<p>Hermannpost is a modified version of the Matrix-Client <a target="new" href="https://fluffychat.im/de/">FluffyChat</a> for use in our school.</p>
<p>For detailed information about the reasons behind the modifications follow this link (German): <a target="new" href="https://hermannschule.de/hermannpost.html">https://hermannschule.de/hermannpost.html</a>.</p>

<p>For a more complete information please visit the original <a target="new" href="https://gitlab.com/ChristianPauly/fluffychat-flutter">FluffyChat .readme File</a>.</p>

<p>You can download and install this client from <a target="new" href="https://play.google.com/store/apps/details?id=de.hermannschule.hermannpost">Google Play Store</a>. However, you won't be able to do anything with it without an account on our server, a QR-key and a PIN for it.</p>

# Use

We use this client with a non federated matrix server for our primary school. The goal is to make it as easy as possible for our school and pupils/parents to communicate with each other, while providing as much privacy, security and moderation as possible.

As administration and moderation tool we use <a target="new" href="https://github.com/devture/matrix-corporal">Matrix Corporal</a>. We create accounts and assign them to moderated rooms generating a policy-config.json file with Excel using VBA. Excel uses <a target="new"  href="https://www.openssl.org/">Openssl</a> to encrypt and <a target="new" href="https://github.com/zint/zint">zint</a> to generate the QR-codes.

# If you want to use it

You can adapt the client to use it with any server.
You will need your own AES256-CBC key to decrypt the QR-Codes (and to encrypt them with whatever tool you use to generate them). At some point I might tidy up our Excel script and publish it if I find the time. If you are going down this path, contact me and I'll send you the actual Excel file with the scripts.
  
# Fluffychat version:

0.26.1

# Modifications of the client in order to increase accessibility, moderation, safety and privacy

 * login only with encrypted QR-key and PIN possible (forces our users to use our client and increases server access protection)
 * no access to directory, new chat only possible using a QR-invitation from another user - this is to prevent non consensual communication
 * confirmation dialog after scanning the QR-invitation showing a big avatar of the inviting user
 * QR-Code content encrypted with AES256-CBC key
 * state messages (join/leave and similar) are not displayed in order to: 1. avoid exposure of {username} in chat rooms with many users reading 2. eliminate long clusters with state messages
 * modified group details page eliminating list with group members in order to avoid user contacts exposure
 * simplyfied settings menu, leaving out options that might be confusing for our target group
 * additional log out button in the drawer in order to facilitate access and make easier to switch between accounts with one device
 * privacy information button in first screen, additional button in the drawer in order to facilitate access
 * "leave" menu item is disabled in direct chats (pupils leaving direct chats is causing a lot of trouble)
 * avatars are a bit bigger than in FluffyChat
 * sent photos are named with the user's mxid and a time stamp in order to facilitate our teacher's work correcting exercises and sending feedback to our pupils
 * screenshots are blocked in order to enhance privacy (unfortunately only possible with the android version)
 * features only accessible for teacher accounts:
  - Video conference call
  - ignore / unignore implemented in user bottom sheet
  - share / download images
 * new: emoji statuses (experimental based on Krilles experiment with this)
 * eventually something that I might have forgotten to list
 
## How to build

Follow instructions found in the FluffyChat .readme file.
Additionally you'll need a folder /keys/ in the assets folder with a keyaes256cbc.txt file with your AES256-CBC key and a ivaes256cbc.txt file with your initialization vector

## Contributions

I learned flutter for this, so expect unnecessary complexity in my code modifications - after all, I'm a primary school teacher. If you are interested in the approach, have better solutions and would like to share them, you're welcome!

# Special thanks to

* <a target="new" href="https://ko-fi.com/krille">Christian Pauly</a>, Sorunome, MTRNord and the rest of the FluffyChat team for their great work and their friendly and encouraging support of open source
* The <a target="new" href="https://matrix.org">Matrix</a> project
* Slavi Pantaleev for <a target="new" href="https://github.com/devture/matrix-corporal">Matrix Corporal</a>, and for his quick and friendly support for the community
* <a target="new" href="https://www.openssl.org/">OpenSSL</a> for their open source encryption tools
* <a target="new" href="https://github.com/zint/zint">zint</a> for this awesome open source QR-code generating tool
* <a target="new" href="https://milan-ihl.de/edv">Milan Ihl</a> for his expert backend support, his support with the iOS builds, and his comitted support of open source projects
* <a target="new" href="https://famedly.com/">Famedly</a> for their contribution to open source with their SDK
